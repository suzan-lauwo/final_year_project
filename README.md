#Server-Load load balancing scheme for Data Centres

##Implementation of the design and evaluation of a load balancing scheme for heavily loaded machines

###Open the project file using the network simulator OPNET Modeler 14.5 to run the project

###View results statistics from the view result option under DES

###Only consider results for the scenarios appended with either SCEN1 (for scenario one results) or SCEN2 (for scenario two results)